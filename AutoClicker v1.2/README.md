**1.Features:**
            <p>Updated UI Color scheme
            Reformated Window from Height: 450, Width: 800<br />
            Reformated Layout objects: Rectangles, Textboxs</p>
	
**2.Fixed bugs:** 
            <p>Clicking stop click more than once doesn't disable single click button, or break clock and click counter</p>
	
**3.Added:**
            <p>user can now use ctrl + x or ctrl + z to stop application instead of clicking stop click button 
            ***( however you must click anywhere on the application first)***</p>
