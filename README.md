# Timed Clicker
 WPF App modeled after MVVM Pattern

 This application is intended to simulate mouse clicking for games that allow it.

 Future updates to be implemented are:

 -Utilization of the ViewModel and Model files, to make the application more flexible.
 -Enhancement of the UI.
 -As well as the additional add-ons such as a Double-Click feature, Background color changes, etc.
